var express = require('express');
var admin = require("firebase-admin");
var bodyParser = require("body-parser")
var serviceAccount = require("./serviceAccountKey.json");
var app = express();

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://vue-tutorial-6e917.firebaseio.com",
  // databaseURL: "https://pntu-dist.firebaseio.com"
});
var db = admin.firestore();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

app.get('/userlist', function (req, res, next) {
  var user = []
  db.collection("user").get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
      user.push({
        first_name: doc.data().first_name,
        last_name: doc.data().last_name,
        group: doc.data().group,
        role: doc.data().role,
        email: doc.data().email,
        uid: doc.id,
      })
    })
    res.send(user)
  })
});

app.post('/add_user', function (req, res, next) {
  admin.auth().createUser({
    email: req.body.email,
    password: req.body.password
  }).then((userRecord) => {
    var docRef = db.collection("user").doc(userRecord.uid)
    console.log(userRecord)
    docRef.set({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      group: req.body.group,
      role: req.body.role,
      email: req.body.email
    })
    res.json(userRecord.uid)
  }).catch((err) => {
    res.send(err)
  })
});

app.post('/delete_user', function (req, res, next) {
  admin.auth().deleteUser(req.body.uid).then(() => {
    res.json("deleted")
  }).catch((err) => {
    res.send(err)
  })
  db.collection("user").doc(req.body.uid).delete()
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
