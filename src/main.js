import Vue from 'vue';
import Vuetify from 'vuetify';
import VuetifyConfirm from 'vuetify-confirm';
import App from './App.vue';
import router from './router';
import store from './store';
import 'vuetify/dist/vuetify.min.css';
import firebaseConfig from './config/firebase.js';
import firebase from 'firebase';
import 'firebase/firestore';
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use(Vuetify);
Vue.use(VuetifyConfirm);
Vue.use(CKEditor);
Vue.config.productionTip = false;
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
Vue.$db = db;


new Vue({
  router,
  store,
  beforeCreate() {
    const vm = this;
    firebase.auth().onAuthStateChanged((user) => {
      vm.$store.dispatch('state_changed', user);
    });
  },
  created() {
    this.$store.dispatch('get_course');
  },
  render: h => h(App),
}).$mount('#app');
