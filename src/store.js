import Vue from 'vue';
import Vuex from 'vuex';
import userModule from './store/user.js';
import generalModule from './store/general.js';
import courseModule from './store/course.js';
import adminModule from './store/admin.js';
import testModule from './store/test.js';


Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    userModule,
    generalModule,
    courseModule,
    adminModule,
    testModule,
  },
});
