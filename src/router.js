import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Course from './views/Course.vue';
import Test from './views/Test.vue';
import Login from './views/Login.vue';
import Signup from './views/Signup.vue';
import Profile from './views/Profile.vue';
import CourseItem from './views/CourseItem.vue';
import CKEditor from './views/CKEditor.vue';
import Store from './store';
import EditCourse from './views/EditCourse.vue';
import EditTest from './views/EditTest.vue'

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/editor',
    name: 'editor',
    component: CKEditor,
  },
  {
    path: '/course',
    name: 'courses',
    component: Course,
    beforeEnter: AuthGuard,

  },
  {
    path: '/test',
    name: 'test',
    component: Test,
    beforeEnter: AuthGuard,

  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/signup',
    name: 'signup',
    component: Signup,
  },
  {
    path: '/course/:id',
    name: 'course',
    component: CourseItem,

  },
  {
    path: '/course/:id/edit',
    name: 'edit_course',
    component: EditCourse,
    props: true,

  },
  {
    path: '/test/:id/edit',
    name: 'edit_test',
    component: EditTest,
    props: true,

  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    beforeEnter: AuthGuard,
  },
  ],
  mode: 'history',
});

function AuthGuard(from, to, next) {
  if (Store.getters.getAuth) {
    next();
  } else {
    next('/login');
  }
}
