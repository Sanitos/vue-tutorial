import firebase from 'firebase';
import Vue from 'vue';

export default {
  state: {
    user: {
      isAuthenticated: false,
      uid: null,
      firstName: null,
      lastName: null,
      role: null,
      group: null,
    },
  },
  mutations: {
    set_user(state, payload) {
      state.user = {
        isAuthenticated: true,
        uid: payload.uid,
        firstName: payload.firstName,
        lastName: payload.lastName,
        role: payload.role,
        group: payload.group,
      };
    },
    unset_user(state) {
      state.user = {
        isAuthenticated: false,
        uid: null,
        firstName: null,
        lastName: null,
      };
    },
    set_auth(state) {
      state.user.isAuthenticated = true;
    },
  },
  actions: {
    signup({
      commit,
    }, payload) {
      commit('set_processing', true);
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then((user) => {
          const userRef = Vue.$db.collection('user');
          userRef.doc(user.user.uid).set({
            first_name: payload.first_name,
            last_name: payload.last_name,
            role: 'student',
            group: payload.group,
            email: payload.email,
          });
          commit('set_processing', false);
        })
        .catch((error) => {
          commit('set_processing', false);
          commit('set_error', error.message);
        });
    },
    login({
      commit,
    }, payload) {
      commit('set_processing', true);
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then((user) => {
          commit('set_processing', false);
          console.log('login');
        })
        .catch((error) => {
          commit('set_processing', false);
          commit('set_error', error.message);
        });
    },
    signout() {
      firebase.auth().signOut();
    },
    state_changed({
      commit,
    }, payload) {
      if (payload) {
        commit('set_auth');
        const userRef = Vue.$db.collection('user').doc(payload.uid);
        userRef.get().then((doc) => {
          commit('set_user', {
            uid: payload.uid,
            firstName: doc.data().first_name,
            lastName: doc.data().last_name,
            role: doc.data().role,
            group: doc.data().group,
          });
        }).catch((error) => {
          console.log(error);
        });
      } else {
        commit('unset_user');
      }
    },
  },
  getters: {
    getUID: state => state.user.uid,
    getAuth: state => state.user.isAuthenticated,
    getUser: state => state.user,

  },
};
