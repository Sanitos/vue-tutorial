import Vue from 'vue';
import _ from 'lodash';
import firebase from 'firebase';

export default {
  state: {
    courses: [],
    groupList: [],
  },
  mutations: {
    set_course(state, payload) {
      state.courses = payload;
    },
    delete_course(state, payload) {
      state.courses.splice(_.findIndex(state.courses, {
        id: payload,
      }), 1);
    },
    add_course(state, payload) {
      state.courses.push(payload);
    },
    update_course(state, payload) {
      const courseUpdate = payload;
      state.courses.splice(_.findIndex(state.courses, {
        id: courseUpdate.id,
      }), 1, courseUpdate);
    },
    set_groupList(state, payload) {
      state.groupList = payload;
    },
  },
  actions: {
    add_course({
      commit,
    }, payload) {
      commit('set_processing', true);
      // var courseRef = Vue.$db.collection("courses")
      const courseRef = Vue.$db.collection('courses');
      courseRef.doc(payload.id).set({
        id: payload.id,
        title: payload.title,
        desc: payload.desc,
        group: payload.group,
        author: payload.author,

      });
      Vue.$db.collection('course_content').doc(payload.id).set({});
      commit('add_course', {
        id: payload.id,
        title: payload.title,
        desc: payload.desc,
        group: payload.group,
        author: payload.author,

      });
      commit('set_processing', false);
    },
    delete_course({
      commit,
    }, payload) {
      const courseRef = Vue.$db.collection('courses');
      courseRef.doc(payload.id).delete().then(() => {
        commit('delete_course', payload.id);
        Vue.$db.collection('course_content').doc(payload.id).delete();
      }).catch((err) => {
        console.log(err);
      });
    },
    get_groupList({
      commit,
    }) {
      const groupRef = Vue.$db.collection('admin_info').doc('group_list');
      groupRef.get().then((doc) => {
        commit('set_groupList', doc.data().list);
      });
    },
    get_course({
      commit,
    }) {
      var courses = [];
      Vue.$db.collection('courses').get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          courses.push(doc.data());
        });
      });
      commit('set_course', courses);
    },
    add_part({
      commit,
    }, payload) {
      const courseRef = Vue.$db.collection('course_content').doc(payload.id);
      const course = payload.course;
      let partArr = [];
      if (course.part != null) {
        partArr = course.part;
      }
      partArr.push(payload.part);
      course.part = partArr;
      commit('update_course', course);
      courseRef.update({
        lessons: firebase.firestore.FieldValue.arrayUnion(payload.part),
      });
    },
    set_part({
      commit,
    }, payload) {
      const partRef = Vue.$db.collection('course_content').doc(payload.id);
      partRef.get().then((doc) => {
        const updateCourse = payload.course;
        updateCourse.part = doc.data().lessons;
        commit('update_course', updateCourse);
      });
    },
  },
  getters: {
    getCourses: state => state.courses,
    getGroupList: state => state.groupList,

  },
};
