import axios from 'axios';
import _ from 'lodash';

export default {
  state: {
    userList: [],
  },
  mutations: {
    set_userList(state, payload) {
      state.userList = payload;
    },
    add_user_to_list(state, payload) {
      state.userList.push(payload);
    },
    delete_user_from_list(state, payload) {
      state.userList.splice(_.findIndex(state.userList, {
        uid: payload,
      }), 1);
    },
  },
  actions: {
    get_userList_server({
      commit,
      state,
    }) {
      console.log(state.userList.length);
      if (state.userList.length == 0) {
        axios.get('http://localhost:3000/userlist').then((response) => {
          commit('set_userList', response.data);
        });
      }
    },
    add_user({
      commit,
    }, payload) {
      axios.post('http://localhost:3000/add_user', {
        email: payload.email,
        password: payload.passwrod,
        first_name: payload.first_name,
        last_name: payload.last_name,
        role: payload.role,
        group: payload.group,

      }).then((response) => {
        commit('add_user_to_list', {
          email: payload.email,
          uid: response.uid,
          first_name: payload.first_name,
          last_name: payload.last_name,
          role: payload.role,
          group: payload.group,
        });
      });
    },
    delete_user({
      commit,
    }, payload) {
      axios.post('http://localhost:3000/delete_user', {
        uid: payload.uid,
      });
      commit('delete_user_from_list', payload.uid);
    },
  },
  getters: {
    getUserList: state => state.userList,
  },
};
