import Vue from 'vue';

export default {
  state: {
      testList: []
  },
  mutations: {
      set_testList(state, payload){
        state.testList = payload
      },
      add_testList(state, payload){
          state.testList.push(payload)
      }
  },
  actions: {
      get_testList({commit, state}){
          if(state.testList.length == 0){
              var test = []
              Vue.$db.collection("test").get().then((querySnapshot)=>{
                  querySnapshot.forEach((doc) => {
                      test.push(doc.data())
                  });
                  commit("set_testList", test)
              })
          }
      },
      add_test({commit}, payload){
          let testRef = Vue.$db.collection("test").doc(payload.id)
          let testObj = {
            id:payload.id,
            author: payload.author,
            title: payload.title,
            desc: payload.desc,
            group: payload.group
          }
          testRef.set(testObj)
          commit("add_testList", testObj)
      }
  },
  getters: {
      getTestList: (state) => state.testList
  },
};
